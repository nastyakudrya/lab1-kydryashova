package impls

import interfaces.Functions

/**
 * Класс, реализующий интерфейс Functions
 */
class FunctionsImpl : Functions {
    override fun calculate(a: Int, b: Int) = a + b

    override fun substringCounter(s: String, sub: String): Int {
        return s.split(sub).count() - 1
    }

    override fun splitterSorter(s: String, sub: String): List<String> {
        return s.split(sub)
    }

    override fun uniqueCounter(s: String, sub: String): Map<String, Int> {
        val r = mutableMapOf<String, Int>()
        for (i in s.split(sub)) {
            if (r.contains(i)) {
                r[i] = r.getValue(i).inc()
            } else {
                r += Pair(i, 1)
            }
        }
        return r
    }

    override fun isPalindrome(s: String): Boolean {
        val r = StringBuilder(s)
        val reverse = r.reverse().toString()
        if (s == "") {
            return false
        }
        return s.equals(reverse, ignoreCase = false)
    }

    override fun invert(s: String): String {
        val r = StringBuilder(s)
        return r.reverse().toString()
    }
}
